﻿using System;
using Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RobotMapper;

namespace Test
{
    [TestClass]
    public class 测试临时生效策略
    {
        [TestMethod]
        public void 测试全局忽略临时绑定()
        {
            //配置全局忽略
            Mapper.Initialize(creator =>
            {
                creator.CreatMap<User, DTO.User>(config =>
                {
                    config.Ignore(x => x.Code);
                });
            });
            var user = TestHelper.创建自引用User();
            //但是本次映射却绑定
            var newuser = user.RobotMap<User, DTO.User>(config =>
            {
                config.Bind(x => x.Code, y => y.Code);
            });
            Assert.AreEqual(user.Code, newuser.Code);
            Assert.IsNotNull(user.Code);
        }

        [TestMethod]
        public void 测试全局绑定临时忽略()
        {
            //配置全局绑定
            Mapper.Initialize(creator =>
            {
                creator.CreatMap<User, DTO.User>(config =>
                {
                    config.Bind(x => x.Code, y => y.Code);
                });
            });
            var user = TestHelper.创建自引用User();
            //但是本次映射却忽略
            var newuser = user.RobotMap<User, DTO.User>(config =>
            {
                config.Ignore(y => y.Code);
            });
            Assert.IsNull(newuser.Code);
        }

        [TestMethod]
        public void 测试全局绑定临时绑定()
        {
            //配置全局绑定
            Mapper.Initialize(creator =>
            {
                creator.CreatMap<User, DTO.User>(config =>
                {
                    config.Bind(x => x.Code, y => y.Code);
                });
            });
            var user = TestHelper.创建自引用User();
            //但是本次临时重新绑定
            var newuser = user.RobotMap<User, DTO.User>(config =>
            {
                config.Bind(x => x.User12.Name, y => y.Code);
            });
            Assert.AreEqual(user.User12.Name, newuser.Code);
            Assert.IsNotNull(newuser.Code);
        }

        [TestMethod]
        public void 测试全局忽略临时忽略()
        {
            //配置全局忽略
            Mapper.Initialize(creator =>
            {
                creator.CreatMap<User, DTO.User>(config =>
                {
                    config.Ignore(x => x.Code);
                });
            });
            var user = TestHelper.创建自引用User();
            //但是本次映射却绑定
            var newuser = user.RobotMap<User, DTO.User>(config =>
            {
                config.Ignore(x => x.Department);
            });
            Assert.IsNull(newuser.Department);
            Assert.IsNotNull(newuser.Code);
        }

        [TestMethod]
        public void 测试两次临时忽略互不影响()
        {
            var user = TestHelper.创建自引用User();
            //本次忽略
            var newuser = user.RobotMap<User, DTO.User>(config =>
            {
                config.Ignore(x => x.Department);
            });
            Assert.IsNull(newuser.Department);
            Assert.IsNotNull(newuser.Code);

            //再次重新配置忽略
            var newuser2 = user.RobotMap<User, DTO.User>(config =>
            {
                config.Ignore(x => x.Code);
            });
            Assert.IsNull(newuser2.Code);
            Assert.AreEqual(user.Department, newuser2.Department);
            Assert.IsNotNull(newuser2.Department);
        }

        [TestMethod]
        public void 测试两次临时绑定互不影响()
        {
            var user = TestHelper.创建自引用User();
            //本次绑定
            var newuser = user.RobotMap<User, DTO.User>(config =>
            {
                config.Bind(x => x.User12.Name, y => y.Code);
            });
            Assert.AreEqual(user.User12.Name, newuser.Code);
            Assert.IsNotNull(newuser.Code);

            //再次重新配置绑定
            var newuser2 = user.RobotMap<User, DTO.User>(config =>
            {
                config.Bind(x => x.User12.Name, y => y.Name);
            });
            Assert.AreEqual(user.User12.Name, newuser2.Name);
            Assert.AreEqual(user.Code, newuser2.Code);
        }
    }
}
