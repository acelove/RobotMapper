﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Entity;
using System.Collections.Generic;
using RobotMapper;
using System.Reflection;
using System.Reflection.Emit;
using System.Linq;
using System.Collections;

namespace Test
{
    [TestClass]
    public class 测试用例
    {
        [TestMethod]
        public void 扁平化测试()
        {
            Mapper.Initialize(creator => { creator.ClearConfig(); });
            var user = TestHelper.创建扁平化User();
            var newuser = user.RobotMap<User, DTO.User>();
            Assert.AreEqual(user.Code, newuser.Code);
            Assert.AreEqual(user.Id, newuser.Id);
            Assert.AreEqual(user.Department, newuser.Department);
            Assert.AreEqual(user.Name, newuser.Name);
        }

        [TestMethod]
        public void 测试循环引用()
        {
            Mapper.Initialize(creator => { creator.ClearConfig(); });
            var user = TestHelper.创建循环引用User();
            var newuser = user.RobotMap<User, DTO.User>();
            Assert.AreEqual(user.Code, newuser.Code);
            Assert.AreEqual(user.Id, newuser.Id);
            Assert.AreEqual(user.Department, newuser.Department);
            Assert.AreEqual(user.Name, newuser.Name);
            Assert.IsTrue(newuser.Roles.Count == 1);
        }

        [TestMethod]
        public void 压力测试()
        {
            var user = TestHelper.创建循环引用User();
            for (int i = 1; i <= 100000; i++)
            {
                var newuser = user.RobotMap<User, DTO.User>();
            }
        }

        [TestMethod]
        public void 批量映射()
        {
            List<User> users = new List<User>();
            for (int i = 1; i <= 10000; i++)
            {
                var user = TestHelper.创建循环引用User();
                users.Add(user);
            }
            var newusers = users.RobotMap<User, DTO.User>();
        }

        [TestMethod]
        public void 复杂对象映射ModelObject()
        {
            var oldObject = TestHelper.创建复杂对象ModelObject();
            var newObject = oldObject.RobotMap<ModelObject, DTO.ModelObject>();
        }

        [TestMethod]
        public void 映射Order()
        {
            var oldObject = TestHelper.GetOrder();
            var newObject = oldObject.RobotMap<Order, DTO.Order>();
            Assert.AreEqual(newObject.CustomerName, "George Costanza");
        }

        [TestMethod]
        public void 映射含可空属性对象()
        {
            var oldObject = TestHelper.GetNullAbleObject();
            var newObject = oldObject.RobotMap<NullAbleObject, DTO.NullAbleObject>();
            Assert.AreEqual(0, newObject.Money);
            Assert.AreEqual(oldObject.Age, newObject.Age);
        }

        [TestMethod]
        public void 枚举测试()
        {
            var oldObject = TestHelper.创建含枚举对象();
            var newObject = oldObject.RobotMap<EnumObject, DTO.EnumObject>();
        }

        [TestMethod]
        public void 测试IListObject()
        {
            var oldObject = TestHelper.GetIListObject();
            var newObject = oldObject.RobotMap<IListObject, DTO.IListObject>();
            Assert.IsTrue(newObject.StrList.Count == 2);
            Assert.IsTrue(newObject.StrList2.Count == 2);
            Assert.IsTrue(newObject.StrList3.Count() == 2);
            Assert.IsTrue(newObject.Colors.Count() == 2);
            Assert.IsTrue(newObject.Users.Count() == 1);
        }
        
        [TestMethod]
        public void 测试ArrayObject()
        {
            var oldObject = TestHelper.GetArrayObject();
            var newObject = oldObject.RobotMap<ArrayObject, DTO.ArrayObject>();
            Assert.IsTrue(newObject.Strs.Length == 2);
            Assert.IsTrue(newObject.Users.Length == 1);
        }

        [TestMethod]
        public void 压力测试创建对象()
        {
            var user = Activator.CreateInstance(typeof(User));
            User newUser = user as User;
            newUser.Name = "qwe";
            Console.WriteLine((user as User).Name);
        }
    }
}
