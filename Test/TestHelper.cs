﻿using Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    public static class TestHelper
    {
        public static User 创建扁平化User()
        {
            Entity.User user = new User()
            {
                Code = "Liuhai",
                Id = "hai.liu",
                Department = "部门1",
                Name = "海哥",
            };
            return user;
        }

        public static User 创建自引用User()
        {
            Entity.User user = new User()
            {
                Code = "Liuhai",
                Id = "hai.liu",
                Department = "部门1",
                Name = "海哥",
                User12=new User()
                {
                    Name="测试Name",
                }
            };
            return user;
        }

        public static User 创建循环引用User()
        {
            Entity.User user = new User()
            {
                Code = "Liuhai",
                Id = "hai.liu",
                Department = "部门1",
                Name = "海哥",
            };
            user.Roles.Add(new Role()
            {
                Name = "角色1",
                Users = new List<User>() { new User() { Name = "SB" } }
            });

            return user;
        }

        public static ModelObject 创建复杂对象ModelObject()
        {
            var _source = new ModelObject
            {
                basedate = new DateTime(2007, 4, 5),
                sub = new ModelSubObject
                {
                    propername = "some name",
                    subsub = new ModelSubSubObject
                    {
                        iamacoolproperty = "cool daddy-o"
                    }
                },
                sub2 = new ModelSubObject
                {
                    propername = "sub 2 name"
                },
                subwithextraname = new ModelSubObject
                {
                    propername = "some other name"
                },
            };
            return _source;
        }

        public static Order GetOrder()
        {
            var customer = new Customer
            {
                Name = "George Costanza"
            };
            var order = new Order
            {
                Customer = customer
            };
            var bosco = new Product
            {
                Name = "Bosco",
                Price = 4.99m
            };
            order.AddOrderLineItem(bosco, 15);
            return order;
        }

        public static NullAbleObject GetNullAbleObject()
        {
            var obj = new NullAbleObject()
            {
                Name = "仙人",
                Age = 100,
                Money = null,
            };
            return obj;
        }

        public static EnumObject 创建含枚举对象()
        {
            return new EnumObject()
            {
                //DTO中不存在的
                Color = Color.Pink,
                //DTO中存在的
                Color2 = Color.Red,
                //可空枚举
                Color3 = Color.Yellow,
                Colors = new List<Color>() { Color.Pink, Color.Yellow },
            };
        }

        public static IListObject GetIListObject()
        {
            IListObject obj = new IListObject()
            {
                StrList = new List<string>() { "Test1", "Test2" },
                StrList2 = new List<string>() { "Test1", "Test2" },
                StrList3 = new List<string>() { "Test1", "Test2" },
                Colors = new List<Color>() { Color.Yellow, Color.Red },
                Users=new List<User>() { new User { Code="TestUserCode"} }
            };
            return obj;
        }

        public static ArrayObject GetArrayObject()
        {
            return new ArrayObject()
            {
                Strs = new string[] { "Test1", "Test2" },
                Users = new User[] { new User() { Code = "Test1212" } }
            };
        }

        public static PropertyCustomClass GetPropertyCustomClass()
        {
            return new PropertyCustomClass()
            {
                Name = "Hello",
                User1 = new User() { Name = "World" },
            };
        }

        public static Children 创建子类实例()
        {
            return new Children()
            {
                BB="Hello World"
            };
        }
    }
}
