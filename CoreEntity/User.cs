﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreEntity
{
    public class User
    {
        public User()
        {
            Roles = new List<Role>();
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Department { get; set; }
        public List<Role> Roles { get; set; }
        public User User12 { get; set; }
    }
}
