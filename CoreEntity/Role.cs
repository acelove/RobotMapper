﻿using System.Collections.Generic;

namespace CoreEntity
{
    public class Role
    {
        public string Name { get; set; }

        public List<User> Users { get; set; }
    }
}