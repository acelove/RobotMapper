﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class EnumObject
    {
        public Color Color { get; set; }
        public Color Color2 { get; set; }
        public Color? Color3 { get; set; }
        public List<Color> Colors { get; set; }
    }

    public enum Color
    {
        Red,
        Yellow,
        Pink,
    }
}
