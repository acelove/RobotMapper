﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class NullAbleObject
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public int? Money { get; set; }
    }
}
