﻿using System.Collections.Generic;

namespace Entity
{
    public class Role
    {
        public string Name { get; set; }

        public List<User> Users { get; set; }
    }
}