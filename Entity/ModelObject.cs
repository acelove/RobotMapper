﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ModelObject
    {
        public DateTime basedate { get; set; }
        public ModelSubObject sub { get; set; }
        public ModelSubObject sub2 { get; set; }
        public ModelSubObject subwithextraname { get; set; }
    }
}
