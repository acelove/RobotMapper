﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ExpTestObject
    {
        public string FirstName { get; set; }
        public ExpTestObjectItem ExpTestObjectItem { get; set; }
    }

    public class ExpTestObjectItem
    {
        public string Name { get; set; }
    }
}
