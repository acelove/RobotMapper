﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ArrayObject
    {
        public string[] Strs { get; set; }
        public User[] Users { get; set; }
    }
}
