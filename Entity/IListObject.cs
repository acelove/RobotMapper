﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class IListObject
    {
        public IList<string> StrList { get; set; }
        public ICollection<string> StrList2 { get; set; }
        public IEnumerable<string> StrList3 { get; set; }
        public IEnumerable<Color> Colors { get; set; }
        public IList<User> Users { get; set; }
    }
}
