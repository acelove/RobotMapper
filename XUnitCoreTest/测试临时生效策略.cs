using CoreEntity;
using RobotMapper;
using System;
using Xunit;
using DTO = CoreDTO;

namespace XUnitCoreTest
{
    public class 测试临时生效策略
    {
        [Fact]
        public void 测试全局忽略临时绑定()
        {
            //配置全局忽略
            Mapper.Initialize(creator =>
            {
                creator.CreatMap<User, DTO.User>(config =>
                {
                    config.Ignore(x => x.Code);
                });
            });
            var user = TestHelper.创建自引用User();
            //但是本次映射却绑定
            var newuser = user.RobotMap<User, DTO.User>(config =>
            {
                config.Bind(x => x.Code, y => y.Code);
            });
            Assert.Equal(user.Code, newuser.Code);
            Assert.NotNull(user.Code);
        }

        [Fact]
        public void 测试全局绑定临时忽略()
        {
            //配置全局绑定
            Mapper.Initialize(creator =>
            {
                creator.CreatMap<User, DTO.User>(config =>
                {
                    config.Bind(x => x.Code, y => y.Code);
                });
            });
            var user = TestHelper.创建自引用User();
            //但是本次映射却忽略
            var newuser = user.RobotMap<User, DTO.User>(config =>
            {
                config.Ignore(y => y.Code);
            });
            Assert.Null(newuser.Code);
        }

        [Fact]
        public void 测试全局绑定临时绑定()
        {
            //配置全局绑定
            Mapper.Initialize(creator =>
            {
                creator.CreatMap<User, DTO.User>(config =>
                {
                    config.Bind(x => x.Code, y => y.Code);
                });
            });
            var user = TestHelper.创建自引用User();
            //但是本次临时重新绑定
            var newuser = user.RobotMap<User, DTO.User>(config =>
            {
                config.Bind(x => x.User12.Name, y => y.Code);
            });
            Assert.Equal(user.User12.Name, newuser.Code);
            Assert.NotNull(newuser.Code);
        }

        [Fact]
        public void 测试全局忽略临时忽略()
        {
            //配置全局忽略
            Mapper.Initialize(creator =>
            {
                creator.CreatMap<User, DTO.User>(config =>
                {
                    config.Ignore(x => x.Code);
                });
            });
            var user = TestHelper.创建自引用User();
            //但是本次映射却绑定
            var newuser = user.RobotMap<User, DTO.User>(config =>
            {
                config.Ignore(x => x.Department);
            });
            Assert.Null(newuser.Department);
            Assert.NotNull(newuser.Code);
        }

        [Fact]
        public void 测试两次临时忽略互不影响()
        {
            var user = TestHelper.创建自引用User();
            //本次忽略
            var newuser = user.RobotMap<User, DTO.User>(config =>
            {
                config.Ignore(x => x.Department);
            });
            Assert.Null(newuser.Department);
            Assert.NotNull(newuser.Code);

            //再次重新配置忽略
            var newuser2 = user.RobotMap<User, DTO.User>(config =>
            {
                config.Ignore(x => x.Code);
            });
            Assert.Null(newuser2.Code);
            Assert.Equal(user.Department, newuser2.Department);
            Assert.NotNull(newuser2.Department);
        }

        [Fact]
        public void 测试两次临时绑定互不影响()
        {
            var user = TestHelper.创建自引用User();
            //本次绑定
            var newuser = user.RobotMap<User, DTO.User>(config =>
            {
                config.Bind(x => x.User12.Name, y => y.Code);
            });
            Assert.Equal(user.User12.Name, newuser.Code);
            Assert.NotNull(newuser.Code);

            //再次重新配置绑定
            var newuser2 = user.RobotMap<User, DTO.User>(config =>
            {
                config.Bind(x => x.User12.Name, y => y.Name);
            });
            Assert.Equal(user.User12.Name, newuser2.Name);
            Assert.Equal(user.Code, newuser2.Code);
        }
    }
}
