﻿using CoreEntity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XUnitCoreTest
{
    public static class TestHelper
    {
        public static User 创建扁平化User()
        {
            CoreEntity.User user = new User()
            {
                Code = "Liuhai",
                Id = "hai.liu",
                Department = "部门1",
                Name = "海哥",
            };
            return user;
        }

        public static User 创建自引用User()
        {
            CoreEntity.User user = new User()
            {
                Code = "Liuhai",
                Id = "hai.liu",
                Department = "部门1",
                Name = "海哥",
                User12=new User()
                {
                    Name="测试Name",
                }
            };
            return user;
        }

        public static User 创建循环引用User()
        {
            CoreEntity.User user = new User()
            {
                Code = "Liuhai",
                Id = "hai.liu",
                Department = "部门1",
                Name = "海哥",
            };
            user.Roles.Add(new Role()
            {
                Name = "角色1",
                Users = new List<User>() { new User() { Name = "SB" } }
            });

            return user;
        }
    }
}
