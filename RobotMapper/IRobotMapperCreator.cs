﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotMapper
{
    public interface IRobotMapperCreator
    {
        /// <summary>
        /// 清除配置
        /// </summary>
        void ClearConfig();
        /// <summary>
        /// 创建映射关系
        /// </summary>
        /// <typeparam name="Source">源类型</typeparam>
        /// <typeparam name="Target">目标类型</typeparam>
        /// <param name="action">映射配置</param>
        void CreatMap<Source, Target>(Action<IRobotMapperConfig<Source, Target>> action = null);
    }
}
