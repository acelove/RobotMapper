﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotMapper
{
    internal class IgnoreRole
    {
        internal Type SourceType { get; set; }

        internal Type TargetType { get; set; }

        private List<string> ignoreMembers = new List<string>();
        internal List<string> IgnoreMembers
        {
            get { return ignoreMembers; }
        }
    }
}
