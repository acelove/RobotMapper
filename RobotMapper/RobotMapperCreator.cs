﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotMapper
{
    internal class RobotMapperCreator : IRobotMapperCreator
    {
        public void ClearConfig()
        {
            GlobalConfig.Clear();
        }

        public void CreatMap<Source, Target>(Action<IRobotMapperConfig<Source, Target>> action = null)
        {
            var config = new RobotMapperConfig<Source, Target>();
            action?.Invoke(config);
        }
    }
}