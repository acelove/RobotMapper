﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace RobotMapper
{
    public interface IRobotMapperConfig<Source, Target>
    {
        void Bind(Func<Source, object> source, Expression<Func<Target, object>> target);

        void BindName(Expression<Func<Source, object>> source, Expression<Func<Target, object>> target);

        void Ignore(Expression<Func<Target, object>> targetIgnore);

        void Ignore(params string[] ignoreProperty);
    }
}
