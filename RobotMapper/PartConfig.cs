﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotMapper
{
    /// <summary>
    /// 局部配置
    /// </summary>
    internal static class PartConfig
    {
        private static List<BindFuncRole> bindFuncRoles = new List<BindFuncRole>();
        internal static List<BindFuncRole> BindFuncRoles
        {
            get { return bindFuncRoles; }
        }

        private static List<BindPropertyRole> bindPropertyRoles = new List<BindPropertyRole>();
        internal static List<BindPropertyRole> BindPropertyRoles
        {
            get { return bindPropertyRoles; }
        }

        private static List<IgnoreRole> ignoreRoles = new List<IgnoreRole>();
        internal static List<IgnoreRole> IgnoreRoles
        {
            get { return ignoreRoles; }
        }

        internal static void Clear()
        {
            BindFuncRoles.Clear();
            BindPropertyRoles.Clear();
            IgnoreRoles.Clear();
        }
    }
}
