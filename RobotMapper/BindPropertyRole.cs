﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotMapper
{
    internal class BindPropertyRole
    {
        internal Type SourceType { get; set; }

        internal Type TargetType { get; set; }

        /// <summary>
        /// 源属性
        /// </summary>
        public string SourcePropertyName { get; set; }
        /// <summary>
        /// 目标属性
        /// </summary>
        public string TargetPropertyName { get; set; }
    }
}
