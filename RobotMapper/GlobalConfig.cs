﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotMapper
{
    internal static class GlobalConfig
    {
        internal static object BindFuncRoleLock = new object();
        internal static object BindPropertyRoleLock = new object();
        internal static object IgnoreRoleLock = new object();

        private static List<BindFuncRole> bindFuncRoles = new List<BindFuncRole>();
        internal static List<BindFuncRole> BindFuncRoles
        {
            get
            {
                lock (BindFuncRoleLock)
                {
                    return bindFuncRoles;
                }
            }
        }

        private static List<BindPropertyRole> bindPropertyRoles = new List<BindPropertyRole>();
        internal static List<BindPropertyRole> BindPropertyRoles
        {
            get
            {
                lock (BindPropertyRoleLock)
                {
                    return bindPropertyRoles;
                }
            }
        }

        private static List<IgnoreRole> ignoreRoles = new List<IgnoreRole>();
        internal static List<IgnoreRole> IgnoreRoles
        {
            get
            {
                lock (IgnoreRoleLock)
                {
                    return ignoreRoles;
                }
            }
        }

        internal static void Clear()
        {
            BindFuncRoles.Clear();
            BindPropertyRoles.Clear();
            IgnoreRoles.Clear();
        }
    }
}
