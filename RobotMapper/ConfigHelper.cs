﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotMapper
{
    internal static class ConfigHelper
    {
        //获取忽略规则
        public static IgnoreRole GetIgnoreRole(Type sType, Type dType, bool isPart)
        {
            lock (GlobalConfig.BindFuncRoleLock)
            {
                if (isPart)
                    return PartConfig.IgnoreRoles.FirstOrDefault(a =>
                       a != null &&
                       a?.SourceType == sType &&
                       a?.TargetType == dType);

                return GlobalConfig.IgnoreRoles.FirstOrDefault(a =>
                     a != null &&
                     a?.SourceType == sType &&
                     a?.TargetType == dType);
            }
        }

        //获取绑定规则
        public static BindFuncRole GetBindFuncRole(Type sType, Type dType, string propertyName, bool isPart)
        {
            lock (GlobalConfig.BindFuncRoleLock)
            {
                if (isPart)
                {
                    var role = PartConfig.BindFuncRoles.FirstOrDefault(a =>
                        a != null &&
                        a?.SourceType == sType &&
                        a?.TargetType == dType &&
                        a?.TargetPropertyName == propertyName);

                    if (role == null)
                        role = PartConfig.BindFuncRoles.FirstOrDefault(a =>
                          a != null &&
                          a.SourceType.IsAssignableFrom(sType) &&
                          a?.TargetType == dType &&
                          a?.TargetPropertyName == propertyName);

                    return role;
                }

                //源到目标
                var gRole = GlobalConfig.BindFuncRoles.FirstOrDefault(a =>
                   a != null &&
                   a?.SourceType == sType &&
                   a?.TargetType == dType &&
                   a?.TargetPropertyName == propertyName);

                //源的父类到目标
                if (gRole == null)
                    gRole = GlobalConfig.BindFuncRoles.FirstOrDefault(a =>
                      a != null &&
                      a.SourceType.IsAssignableFrom(sType) &&
                      a?.TargetType == dType &&
                      a?.TargetPropertyName == propertyName);

                //源到目标的父类
                if (gRole == null)
                    gRole = GlobalConfig.BindFuncRoles.FirstOrDefault(a =>
                      a != null &&
                      a.SourceType == sType &&
                      a.TargetType.IsAssignableFrom(dType) &&
                      a?.TargetPropertyName == propertyName);

                //源的父类到目标的父类
                if (gRole == null)
                    gRole = GlobalConfig.BindFuncRoles.FirstOrDefault(a =>
                      a != null &&
                      a.SourceType.IsAssignableFrom(sType) &&
                      a.TargetType.IsAssignableFrom(dType) &&
                      a?.TargetPropertyName == propertyName);

                return gRole;
            }
        }

        //获取绑定属性
        public static BindPropertyRole GetBindPropertyRole(Type sType, Type dType, string propertyName, bool isPart)
        {
            lock (GlobalConfig.BindPropertyRoleLock)
            {
                if (isPart)
                    return PartConfig.BindPropertyRoles.FirstOrDefault(a =>
                     a != null &&
                     a?.SourceType == sType &&
                     a?.TargetType == dType &&
                     a?.TargetPropertyName == propertyName);

                return GlobalConfig.BindPropertyRoles.FirstOrDefault(a =>
                     a?.SourceType == sType &&
                     a?.TargetType == dType &&
                     a?.TargetPropertyName == propertyName);
            }
        }
    }
}
