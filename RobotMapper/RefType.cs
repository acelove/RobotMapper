﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotMapper
{
    internal class RefType
    {
        public string PropertyName { get; set; }
        public Type RefrenceType { get; set; }
    }
}
