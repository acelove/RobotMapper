﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotMapper
{
    public class SystemIgnoreRole
    {
        public Guid Id { get; set; }
        public Type TheType { get; set; }
        public List<string> IgnoreMembers { get; set; }
    }
}
