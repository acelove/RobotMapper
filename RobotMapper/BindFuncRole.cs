﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotMapper
{
    internal class BindFuncRole
    {
        internal Type SourceType { get; set; }

        internal Type TargetType { get; set; }

        /// <summary>
        /// 生成目标值的规则
        /// </summary>
        public Func<object, object> SourceFunc { get; set; }
        /// <summary>
        /// 目标属性
        /// </summary>
        public string TargetPropertyName { get; set; }
    }
}
