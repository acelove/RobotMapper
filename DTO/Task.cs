﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    /// <summary>
    /// 子任务
    /// </summary>
    public class Task
    {
        public Task()
        {
        }
        
        public long Id { get; set; }
        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { get; set; }
        /// <summary>
        /// 任务内容
        /// </summary>
        public string TaskContent { get; set; }
        /// <summary>
        /// 任务执行人Id
        /// </summary>
        public string ExecutorId { get; set; }
        /// <summary>
        /// 任务状态
        /// </summary>
        public short Status { get; set; }
        /// <summary>
        /// 任务开始时间
        /// </summary>
        public DateTime? StartTime { get; set; }
        /// <summary>
        /// 任务截止时间
        /// </summary>
        public DateTime EndTime { get; set; }
        /// <summary>
        /// 剩余天数
        /// </summary>
        public int RemainingDay
        {
            get
            {
                if (DateTime.Now < EndTime)
                    return (int)(this.EndTime - DateTime.Now).TotalDays;
                else
                    return 0;
            }
        }
        /// <summary>
        /// 所属事项Id
        /// </summary>
        public long BacklogId { get; set; }

        /// <summary>
        /// 所属事项
        /// </summary>
        public MeetingBacklog Backlog { get; set; }
        /// <summary>
        /// 任务执行人
        /// </summary>
        public UserInfo Executor { get; set; }
    }
}
