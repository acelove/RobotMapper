﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class Order
    {
        public string CustomerName { get; set; }
        public decimal Total { get; set; }
    }
}
