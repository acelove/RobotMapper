﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    /// <summary>
    /// 会议纪要待办事项
    /// </summary>
    public class MeetingBacklog : Backlog
    {
        public MeetingBacklog()
        {
            this.Task = new List<Task>();
            this.SourceType = 1;
            this.LinkSourceId = this.Id;
        }
        
        /// <summary>
        /// 事项编号
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// 授权人Id
        /// </summary>
        public string AuthorizedPersonId { get; set; }
        /// <summary>
        /// 责任人Id
        /// </summary>
        public string LiablePersonId
        {
            get { return this.ToPersonId; }
            set { this.ToPersonId = value; }
        }
        /// <summary>
        /// 工作周期
        /// </summary>
        public int WorkCycle { get; set; }
        /// <summary>
        /// 剩余时间/天
        /// </summary>
        public int RemainingDay { get; set; }

        private string _Deliverable;
        /// <summary>
        /// 交付物
        /// </summary>
        public string Deliverable
        {
            get { return _Deliverable; }
            set
            {
                this.Content = string.Format("待办事项【{0}】需完成，交付物为【{1}】", this.Name, value);
                this._Deliverable = value;
            }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 所属会议纪要Id
        /// </summary>
        public long MeetingSummaryId { get; set; }



        /// <summary>
        /// 子任务
        /// </summary>
        public List<Task> Task { get; set; }
        /// <summary>
        /// 授权人
        /// </summary>
        public UserInfo AuthorizedPerson { get; set; }
        /// <summary>
        /// 执行人
        /// </summary>
        public List<UserInfo> Executors { get; set; }
        public List<string> ExecutorIds
        {
            get
            {
                return this.Executors.Select(a => a.UserId).ToList();
            }
            set
            {
                this.Executors.Clear();
                value.ForEach(a => this.Executors.Add(new UserInfo() { UserId = a }));
            }
        }
        /// <summary>
        /// 所属会议纪要
        /// </summary>
        public MeetingSummary MeetingSummary { get; set; }
    }
}
