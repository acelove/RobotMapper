﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    /// <summary>
    /// 部门信息
    /// </summary>
    public class DeptInfo
    {
        /// <summary>
        /// 部门Id
        /// </summary>
        public string DeptId { get; set; }
        /// <summary>
        /// 部门名
        /// </summary>
        public string DeptName { get; set; }
        /// <summary>
        /// 上级部门Id
        /// </summary>
        public string PdeptId { get; set; }
        /// <summary>
        /// 上级部门名
        /// </summary>
        public string PdeptName { get; set; }
        
    }
}
