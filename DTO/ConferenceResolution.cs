﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DTO
{
    /// <summary>
    /// 会议决议
    /// </summary>
    public class ConferenceResolution
    {
        public ConferenceResolution()
        {
            this.ConferenceRanges = new List<UserInfo>();
        }
        
        public long Id { get; set; }
        /// <summary>
        /// 决议编号
        /// </summary>
        public string Number { get; set; }
        /// <summary>
        /// 决议名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 签发人Id
        /// </summary>
        public string ConferencePersonId { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// 决议内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 决议状态 枚举：ConferenceResolutionStatus
        /// </summary>
        public short Status { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 所属会议纪要Id
        /// </summary>
        public long MeetingSummaryId { get; set; }
        /// <summary>
        /// 通知形式Json
        /// </summary>
        public string NoticeTypes { get; set; }
        /// <summary>
        /// 通知形式
        /// </summary>
        public List<short> NoticeTypeEnums { get; set; }

        /// <summary>
        /// 签发范围
        /// </summary>
        public List<UserInfo> ConferenceRanges { get; set; }
        public List<string> ConferenceRangeIds
        {
            get
            {
                return this.ConferenceRanges.Select(a => a.UserId).ToList();
            }
            set
            {
                this.ConferenceRanges.Clear();
                value.ForEach(a => this.ConferenceRanges.Add(new UserInfo() { UserId = a }));
            }
        }

        /// <summary>
        /// 所属会议纪要
        /// </summary>
        public MeetingSummary MeetingSummary { get; set; }
        /// <summary>
        /// 签发人
        /// </summary>
        public UserInfo ConferencePerson { get; set; }
    }
}
