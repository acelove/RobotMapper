﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DTO
{
    /// <summary>
    /// 通知
    /// </summary>
    public class Message
    {
        public Message()
        {
            this.NoticeUsers = new List<UserInfo>();
        }

        public long Id { get; set; }
        /// <summary>
        /// 通知名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 通知内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 通知形式Json
        /// </summary>
        public string NoticeTypes { get; set; }
        /// <summary>
        /// 通知形式 枚举 NoticeType
        /// </summary>
        public List<short> NoticeTypeEnums { get; set; }
        /// <summary>
        /// 是否已读
        /// </summary>
        public bool IsRead { get; set; }
        /// <summary>
        /// 来源Id
        /// </summary>
        public long SourceId { get; set; }
        /// <summary>
        /// 来源类型 枚举 SourceType
        /// </summary>
        public short SourceType { get; set; }

        /// <summary>
        /// 需要通知的成员
        /// </summary>
        public List<UserInfo> NoticeUsers { get; set; }
        public List<string> NoticeUserIds
        {
            get
            {
                return this.NoticeUsers.Select(a => a.UserId).ToList();
            }
            set
            {
                this.NoticeUsers.Clear();
                value.ForEach(a => this.NoticeUsers.Add(new UserInfo() { UserId = a }));
            }
        }
    }
}
