﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class Parent
    {
        public string AA { get; set; }
    }

    public class Children : Parent
    {
        public string AAA { get; set; }
    }
}
