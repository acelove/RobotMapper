﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace DTO
{
    /// <summary>
    /// 用户
    /// </summary>
    public class UserInfo
    {
        public UserInfo()
        {
            this.ParticipantMeetings = new List<MeetingSummary>();
            this.ModeratorMeetings = new List<MeetingSummary>();
            this.RecordeMeetings = new List<MeetingSummary>();
            this.IssueCreatMeetings = new List<MeetingSummary>();
            this.Tasks = new List<Task>();
            this.AuthorizedBacklogs = new List<MeetingBacklog>();
            this.ConferenceResolutions = new List<ConferenceResolution>();
            this.ExecutBacklogs = new List<MeetingBacklog>();
            this.Messages = new List<Message>();
            this.NoticeMeetings = new List<MeetingSummary>();
        }


        /// <summary>
        /// 员工ID
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 中文名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 部门ID
        /// </summary>
        public string DeptId { get; set; }
        /// <summary>
        /// 部门中文名
        /// </summary>
        public string DeptName { get; set; }
        /// <summary>
        /// IT账号
        /// </summary>
        public string ItCode { get; set; }
        /// <summary>
        /// 移动电话
        /// </summary>
        public string Mobile { get; set;  }
        /// <summary>
        /// 邮件地址
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 岗位Id
        /// </summary>
        public string PostId { get; set; }
        /// <summary>
        /// 岗位中文名
        /// </summary>
        public string PostName { get; set; }
        /// <summary>
        /// 是否被删除
        /// </summary>
        public bool IsDeleted { get; set; }
        /// <summary>
        /// 钉钉Id
        /// </summary>
        public string DdId { get; set; }
        



        /// <summary>
        /// 用户参与的所有会议纪要
        /// </summary>
        public List<MeetingSummary> ParticipantMeetings { get; set; }
        /// <summary>
        /// 用户有关签发的所有会议纪要
        /// </summary>
        public List<MeetingSummary> NoticeMeetings { get; set; }
        /// <summary>
        /// 主持的会议纪要
        /// </summary>
        public List<MeetingSummary> ModeratorMeetings { get; set; }
        /// <summary>
        /// 记录的会议纪要
        /// </summary>
        public List<MeetingSummary> RecordeMeetings { get; set; }
        /// <summary>
        /// 签发的会议纪要
        /// </summary>
        public List<MeetingSummary> IssueCreatMeetings { get; set; }
        /// <summary>
        /// 用户需要执行的所有子Task
        /// </summary>
        public List<Task> Tasks { get; set; }
        /// <summary>
        /// 授权的Backlog
        /// </summary>
        public List<MeetingBacklog> AuthorizedBacklogs { get; set; }
        /// <summary>
        /// 签发的会议决议
        /// </summary>
        public List<ConferenceResolution> ConferenceResolutions { get; set; }
        /// <summary>
        /// 用户所有的提醒
        /// </summary>
        public List<Message> Messages { get; set; }
        /// <summary>
        /// 用户执行的Backlog
        /// </summary>
        public List<MeetingBacklog> ExecutBacklogs { get; set; }
        /// <summary>
        /// 需要被签发的会议决议
        /// </summary>
        public List<ConferenceResolution> NoticeResolutions { get; set; }
        /// <summary>
        /// 所有待办
        /// </summary>
        public List<Backlog> Backlogs { get; set; }
        /// <summary>
        /// 操作记录
        /// </summary>
        public List<OperationRecord> OperationRecords { get; set; }
    }
}
