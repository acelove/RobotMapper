﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DTO
{
    /// <summary>
    /// 会议纪要
    /// </summary>
    public class MeetingSummary
    {
        public MeetingSummary()
        {
            this.Participants = new List<UserInfo>();
            this.ConferenceResolutions = new List<ConferenceResolution>();
            this.MeetingBacklogs = new List<MeetingBacklog>();
            this.NoticeRanges = new List<UserInfo>();
        }
        
        public long Id { get; set; }
        /// <summary>
        /// 会议编号
        /// </summary>
        public string Number { get; set; }
        /// <summary>
        /// 会议名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 密级 枚举 ClassifiedLevel
        /// </summary>
        public short Level { get; set; }
        /// <summary>
        /// 会议时间
        /// </summary>
        public DateTime Time { get; set; }
        /// <summary>
        /// 会议形式 枚举MeetingType
        /// </summary>
        public short Type { get; set; }
        /// <summary>
        /// 会议地点
        /// </summary>
        public string Place { get; set; }
        /// <summary>
        /// 会议主持者Id
        /// </summary>
        public string ModeratorId { get; set; }
        /// <summary>
        /// 会议记录者Id
        /// </summary>
        public string RecorderId { get; set; }
        /// <summary>
        /// 需要确认
        /// </summary>
        public bool NeedConfirm { get; set; }
        /// <summary>
        /// 签发人Id
        /// </summary>
        public string IssueCreatorId { get; set; }
        /// <summary>
        /// 通知形式Json
        /// </summary>
        public string NoticeTypes { get; set; }
        /// <summary>
        /// 通知形式
        /// </summary>
        public List<short> NoticeTypeEnums { get; set; }

  //      public string Remark { get; set; }
        /// <summary>
        /// 备注
        /// <summary>
        /// 签发日期
        /// </summary>
        public DateTime IssueCreateDate { get; set; }
        /// <summary>
        /// 状态 枚举 MeetingSummaryStatus
        /// </summary>
        public short Status { get; set; }
        /// <summary>
        /// 最后更新时间
        /// </summary>
        public DateTime LastUpdateTime { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationTime { get; set; }


        /// <summary>
        /// 会议参与者
        /// </summary>
        public List<UserInfo> Participants { get; set; }
        public List<string> ParticipantIds
        {
            get
            {
                return this.Participants.Select(a => a.UserId).ToList();
            }
            set
            {
                this.Participants.Clear();
                value.ForEach(a => this.Participants.Add(new UserInfo() { UserId = a }));
            }
        }

        /// <summary>
        /// 签发范围
        /// </summary>
        public List<UserInfo> NoticeRanges { get; set; }
        public List<string> NoticeRangeIds
        {
            get
            {
                return this.NoticeRanges.Select(a => a.UserId).ToList();
            }
            set
            {
                this.NoticeRanges.Clear();
                value.ForEach(a => this.NoticeRanges.Add(new UserInfo() { UserId = a }));
            }
        }

        /// <summary>
        /// 会议决议
        /// </summary>
        public List<ConferenceResolution> ConferenceResolutions { get; set; }
        /// <summary>
        /// 待办事项
        /// </summary>
        public List<MeetingBacklog> MeetingBacklogs { get; set; }
        /// <summary>
        /// 会议主持者
        /// </summary>
        public UserInfo Moderator { get; set; }
        /// <summary>
        /// 会议记录者
        /// </summary>
        public UserInfo Recorder { get; set; }
        /// <summary>
        /// 签发人
        /// </summary>
        public UserInfo IssueCreator { get; set; }
    }
}
