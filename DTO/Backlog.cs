﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    /// <summary>
    /// 所有待办事项
    /// </summary>
    public class Backlog
    {
        public long Id { get; set; }

        /// <summary>
        /// 事项名称
        /// </summary>
        public string Name { get; set; }

        public string DisplayName { get; set; }

        /// <summary>
        /// 优先级
        /// </summary>
        public short? Priority { get; set; }

        /// <summary>
        /// 状态 枚举TaskStatus
        /// </summary>
        public short Status { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 关联来源Id
        /// </summary>
        public long LinkSourceId { get; set; }

        /// <summary>
        /// 来源类型 枚举SourceType
        /// </summary>
        public short SourceType { get; set; }
        /// <summary>
        /// 指派对象Id
        /// </summary>
        public string ToPersonId { get; set; }
        /// <summary>
        /// 开始日期
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// 截止日期
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// 完成时间
        /// </summary>
        public DateTime? DoneTime { get; set; }


        /// <summary>
        /// 指派对象
        /// </summary>
        public UserInfo ToPerson { get; set; }
    }
}
