﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class SystemConfig
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public DateTime? LastUpdateTime { get; set; }
    }
}
