﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class OperationRecord
    {
        public long Id { get; set; }

        /// <summary>
        /// 操作对象Id
        /// </summary>
        public long OperationItemId { get; set; }

        /// <summary>
        /// 操作对象类型 枚举 SourceType
        /// </summary>
        public short OperationItemType { get; set; }

        /// <summary>
        /// 操作人Id
        /// </summary>
        public string OperationUserId { get; set; }

        /// <summary>
        /// 操作类型 枚举 OperationType
        /// </summary>
        public short OperationType { get; set; }

        /// <summary>
        /// 操作备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public UserInfo OperationUser { get; set; }
    }
}
