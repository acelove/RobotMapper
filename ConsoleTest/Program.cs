﻿using Entity;
using RobotMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //for (int i = 1; i <= 20; i++)
            //{
            //    InitializeBind1(i);
            //    InitializeBind2(i);
            //}
            //Console.Read();

            Task task1 = new Task(Method);
            Task task2 = new Task(Method);
            Task task3 = new Task(Method);

            task1.Start();
            task2.Start();
            task3.Start();

            Console.Read();
        }

        private static void InitializeBind1(int i)
        {
            Console.WriteLine("InitializeBind1第{0}次运行！", i.ToString());
            Task.Run(() =>
            {
                Mapper.Initialize(creator =>
                {
                    creator.CreatMap<User, DTO.User>(config =>
                    {
                        config.Bind(x => x.User12.Name, y => y.UserName);
                        config.Ignore(y => y.Roles);
                    });
                });

                var user = TestHelper.创建自引用User();
                var newuser = user.RobotMap<User, DTO.User>();
                if (newuser.UserName != user.User12.Name || newuser.UserName == null)
                    Console.WriteLine("InitializeBind1配置失败！");
            });
        }
        private static void InitializeBind2(int i)
        {
            Console.WriteLine("InitializeBind2第{0}次运行！", i.ToString());
            Task.Run(() =>
            {
                Mapper.Initialize(creator =>
                {
                    creator.CreatMap<User, DTO.User>(config =>
                    {
                        config.Ignore(y => y.UserName);
                    });
                });

                var user = TestHelper.创建自引用User();
                var newuser = user.RobotMap<User, DTO.User>();
                if (newuser.UserName != null)
                    Console.WriteLine("InitializeBind2配置失败！");
            });
        }

        public static void Method()
        {
            Console.WriteLine("Start");
            List<User> users = new List<User>();
            for (int i = 1; i <= 10000; i++)
            {
                var user = TestHelper.创建循环引用User();
                users.Add(user);
            }
            var newusers = users.RobotMap<User, DTO.User>();
            Console.WriteLine("End");
        }
    }
}
